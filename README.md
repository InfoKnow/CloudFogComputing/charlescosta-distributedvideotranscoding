# Distran - Distributed transcoding

## Introduction

**Distran - Distributed transcoding** is a project written in Java and Spring Boot[^3] to enable distributed transcoding of video streaming using the standard MPEG-DASH[^1], which employs devices on the edge of the network. It is part of my masters' work at the Computer Science Department of the University of Brasilia. It is a real-world implementation of a simulation published[^2] on the 21st International Workshop on Trust in Agent Societies, where more details about the background theory are described.


## Architecture

The Distran architecture relies on an agent and four roles, as can be seen at the Figure 1.

![Figure 1. Distran main agent roles.](https://gitlab.com/InfoKnow/CloudFogComputing/charlescosta-distributedvideotranscoding/-/raw/98ecb0ca01242e4b1cddb7913af942d7365e0e7a/Workload-Roles__1_.png).

Figure 1. Distran main agent roles.

1. Provider: This role is the original provider, from where the video stream is served to viewers. Provider examples of standard HLS stream providers are Twitch and Daily Motions, but there are many all over the Internet.

2. Broker: Broker role performers are responsible for choosing adequate transcoders among those who are previously registered. This role also informs viewers where they should go to get the video segments.

3. Viewer: Viewer role performers are the final users of the stream provider. Somehow, they could not deal with the bitrate served. Due to its limitations, viewers must rely on Broker's ability to transcode the original stream in lower bitrate versions.

Broker and Viewer are implemented in the [DistranB](distranb) project, since Viewer main classes are implemented in JavaScript at the [Viewer.js](distranb/src/main/resources/static/viewer.js) file and used in the [Play.html](distranb/src/main/resources/templates/play.html) togheter with Hls.js[^4] player.

4. Transcoder: They are those interested in performing the video segment transcoding in exchange for a reward.

Transcoder is implemented in the [DistranT](distrant) project.

4. Directory Facilitator: This is an agent responsible for telling other agents where to find near Brokers. In this project, broker agent locations are set up directly in the Transcoders' configuration files.


## Configuration

Since the projects are Spring Boot projects, they use *application.properties* files to store their configuration.

### Broker e viewer

Brokers need a place to store the M3U8 playlist files, then you have to indicate a place using the *videoDirectory* parameter.

```
videoDirectory=C:/temp/distranb
```

### Transcoder

Transcoders use the FFMPEG[^5], a popular free tool for video manipulation. The video transcoding is made using a FFMPEG local installation, so you have to install FFMPEG in your device and indicate where the *ffmpeg* and the *ffprob* executables were installed. You do so using the *ffmpegPath* and *ffprobPath* parameters in the *application.properties* file.

Transcoders need a place to store the transcoded video segments, then you have to indicate a place using the *videoDirectory* parameter.

Since we do not have a implementation to the Directory Facilitator agent, you have to indicate the Broker URL using the *brokerUrl* parameter.

```
ffmpegPath=C:/Users/user/ffmpeg-4.2.1-win64-static/bin/ffmpeg
ffprobPath=C:/Users/user/ffmpeg-4.2.1-win64-static/bin/ffprob

videoDirectory=C:/temp/distrant

#How to find the broker
brokerUrl=http://localhost:8080/distranbroker
```

## Final considerations

This project was financially supported by the Graduate Dean of the University of Brasilia through the notice DPG 004/2021[^6].

## References

[^1]: https://en.wikipedia.org/wiki/Dynamic_Adaptive_Streaming_over_HTTP

[^2]: C. A. N. Costa, B. Macchiavello, and C. Ghedini Ralha, “Trust and reputation multiagent-driven model for distributed transcoding onfog-edge,” inProc. of 21st International Workshop on Trust in AgentSocieties, 2021. [Online]. Available: https://drive.google.com/file/d/1hg3n4BBTeOUIr9aXecEe-WY9YzDf06pV/view.

[^3]: https://spring.io/projects/spring-boot.

[^4]: https://github.com/video-dev/hls.js/.

[^5]: https://www.ffmpeg.org/.

[^6]: http://dpg.unb.br/images/Edital_DPG_0004-2021.pdf. 


