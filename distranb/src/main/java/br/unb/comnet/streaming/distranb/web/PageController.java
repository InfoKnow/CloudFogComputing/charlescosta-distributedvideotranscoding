package br.unb.comnet.streaming.distranb.web;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import br.unb.comnet.streaming.distranb.model.TaskService;
import br.unb.comnet.streaming.distranb.model.TaskServiceException;
import br.unb.comnet.streaming.distranb.model.TranscoderRepository;
import br.unb.comnet.streaming.distranb.services.FileVideoService;

@Controller
public class PageController {
	private Log log = LogFactory.getLog(PageController.class);
	
	@Autowired
	private TranscoderRepository repo;
	
	@Autowired
	private FileVideoService videoService;
	
	@Autowired
	private TaskService taskService;
	
	@GetMapping("/")
	public String home(Model model) {
		model.addAttribute("jobs", taskService.listTaskNames());
		model.addAttribute("transcoders", repo.listTranscodersInfo());
		model.addAttribute("hlsFile", new HlsFile());
		return "index";
	}
	
	@PostMapping("/")
	public String transcode(@ModelAttribute("hlsFile") HlsFile hlsFile, Model model) {
		try {
			taskService.startATask(hlsFile.getName(), hlsFile.getUrl());
		} catch (TaskServiceException | IOException e) {
			e.printStackTrace();
			return "error";
		}
		return home(model);
	}
	
	@GetMapping("/play/{taskName}")
	public String play(@PathVariable("taskName") String taskName, Model model) {
		try {
			String fileName = taskName + ".m3u8"; 
			Resource source = videoService.getVideoURL(fileName);
			model.addAttribute("mediaType", videoService.getResourceMediaType(source));
			model.addAttribute("fileURL", "/distranbroker/videos/full/"+fileName);
			return "play";			
		} catch (IOException e) {
			log.error("Something wrong has happen >>> {}", e);
			return "error";
		}
	}	

}
