package br.unb.comnet.streaming.distranb.model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.logging.log4j.util.Strings;
import org.springframework.core.io.Resource;

import br.com.tm.repfogagent.trm.Rating;
import br.unb.comnet.streaming.distranb.services.FileVideoService;
import br.unb.comnet.streaming.distranb.services.PlayListBuilder;
import br.unb.comnet.streaming.distranb.services.TaskDistributionUnit;

public class TaskService {
	
	private TaskRepository repoTasks;
	private TranscoderRepository repoTranscoders;
	private FileVideoService fileVideoService;
	
	public TaskService(
		TaskRepository repoTasks,
		TranscoderRepository repoTranscoders,
		FileVideoService fileVideoService			
	) {
		this.repoTasks = repoTasks;
		this.repoTranscoders = repoTranscoders;
		this.fileVideoService = fileVideoService;
	}
	
	public PlayListBuilder startATask(String m3u8Url) throws FileNotFoundException, IOException, TaskServiceException {
		return startATask("job" + repoTasks.numOfTasks(), m3u8Url);
	}
	
	public PlayListBuilder startATask(String taskName, String m3u8Url) throws FileNotFoundException, IOException, TaskServiceException {
		if (Strings.isBlank(taskName)) {
			throw new TaskServiceException("Every task must have a name.");
		}
		if (Strings.isBlank(m3u8Url)) {
			throw new TaskServiceException("A URL to a m3u8 playlist file must be provided.");
		}
		
		URL url = new URL(m3u8Url);
		Resource output = fileVideoService.createFileForWriting(taskName + ".m3u8");
		TaskDistributionUnit unitJob = new TaskDistributionUnit(taskName, fileVideoService, repoTranscoders, url, output);
		repoTasks.addTask(unitJob);
		new Thread(unitJob).start();
		return unitJob;
	}
	
	public Optional<PlayListBuilder> stopATask(String taskName) throws TaskServiceException {
		if (Strings.isBlank(taskName)) {
			throw new TaskServiceException("A task name must be provided.");
		}
		
		Optional<PlayListBuilder> opTask = repoTasks.getTask(taskName);
		if (opTask.isPresent()) {
			opTask.get().stop();
			repoTasks.removeTask(opTask.get().getName());
		}
		return opTask;
	}
	
	public List<String> listTaskNames() {
		return repoTasks.listTasks().stream()
				.map(PlayListBuilder::getName)
					.collect(Collectors.toList());
	}
	
	public void processUtilityFeedback(List<UtilityFeedback> feedbacks) {
		feedbacks.stream().forEach(feedback -> {
			repoTranscoders.getResponsibleFor(feedback.getUrl()).ifPresent(transcoder -> {
				transcoder.addRating(createRating(feedback, transcoder.getName()));
			});
		});
	}
	
	private Rating createRating(UtilityFeedback feedback, String transcoderName) {
		return new Rating(
			transcoderName,
			feedback.getEvaluator(), 
			feedback.getUtility(), 
			feedback.getUtility(),
			feedback.getId(),
			"Transcoding",			
			convert(feedback.getTimeStamp())
		);
	}
	
	private static Date convert(LocalDateTime time) {
		return Date.from(time.atZone(ZoneId.systemDefault()).toInstant());
	}	
}
