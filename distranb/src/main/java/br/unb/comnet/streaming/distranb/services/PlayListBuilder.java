package br.unb.comnet.streaming.distranb.services;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import br.unb.comnet.streaming.distranb.flexM3u8IO.FlexiblePlayListReader;
import br.unb.comnet.streaming.distranb.flexM3u8IO.FlexiblePlayListWriter;

public abstract class PlayListBuilder implements Runnable {
	private Log log = LogFactory.getLog(PlayListBuilder.class);
	
	private final String name;
	
	private FileVideoService fileService;
	private boolean hasToStop = false;	
	private boolean running = false;
	
	private FlexiblePlayListReader reader;
	private FlexiblePlayListWriter writer;	
	
	public String getName() {
		return name;
	}
	
	public boolean isRunning() {
		return running;
	}
	
	public PlayListBuilder(String name, FileVideoService fileService) {
		this.name = name;		
		this.fileService = fileService;
	}
	
	protected FileVideoService getFileService() {
		return fileService;
	}

	protected FlexiblePlayListReader getReader() {
		return reader;
	}

	protected FlexiblePlayListWriter getWriter() {
		return writer;
	}

	public void stop() {
		hasToStop = true;
	}	
	
	@Override
	public void run() {
		log.info("Starting... ");
		running = true;
		
		reader = new FlexiblePlayListReader();
		writer = new FlexiblePlayListWriter();			
		
		initilize();
		
		log.info("Will start the loop...");
		while(!hasToStop) {
			process();
			log.info("looooop...");
		}
		
		running = false;
	}
	
	public abstract void initilize();
	
	public abstract void process();
}
