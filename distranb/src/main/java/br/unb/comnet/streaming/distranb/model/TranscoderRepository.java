package br.unb.comnet.streaming.distranb.model;

import java.util.List;
import java.util.Optional;

public interface TranscoderRepository {
	
	public void add(TranscoderClient transcoder);
	public int size();
	public Optional<TranscoderClient> getTranscoder(int index);
	public Optional<TranscoderClient> getResponsibleFor(String url);
	public List<TranscoderClient> listAllTranscoders();
	public List<TranscoderInfo> listTranscodersInfo();

}
