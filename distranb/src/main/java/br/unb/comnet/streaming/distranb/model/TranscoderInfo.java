package br.unb.comnet.streaming.distranb.model;

import java.io.Serializable;

public class TranscoderInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String name;
	private Integer numOfSegments;
	private Double totalUtility;
	private Double trustworth;
	private Double reliability;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Integer getNumOfSegments() {
		return numOfSegments;
	}
	public void setNumOfSegments(Integer numOfSegments) {
		this.numOfSegments = numOfSegments;
	}
	
	public Double getTotalUtility() {
		return totalUtility;
	}
	public void setTotalUtility(Double totalUtility) {
		this.totalUtility = totalUtility;
	}
	
	public Double getTrustworth() {
		return trustworth;
	}
	public void setTrustworth(Double trustworth) {
		this.trustworth = trustworth;
	}
	
	public Double getReliability() {
		return reliability;
	}
	public void setReliability(Double reliability) {
		this.reliability = reliability;
	}
	
	public TranscoderInfo(
			String name, 
			Integer numOfSegments, 
			Double totalUtility, 
			Double trustworth,
			Double reliability
	) {
		this.name = name;
		this.numOfSegments = numOfSegments;
		this.totalUtility = totalUtility;
		this.trustworth = trustworth;
		this.reliability = reliability;
	}
	
}
