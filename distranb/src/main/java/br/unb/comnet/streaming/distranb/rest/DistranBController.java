package br.unb.comnet.streaming.distranb.rest;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.unb.comnet.streaming.distranb.model.TaskService;
import br.unb.comnet.streaming.distranb.model.TaskServiceException;
import br.unb.comnet.streaming.distranb.model.TranscoderClient;
import br.unb.comnet.streaming.distranb.model.TranscoderRepository;
import br.unb.comnet.streaming.distranb.model.UtilityFeedback;
import br.unb.comnet.streaming.distranb.services.FileVideoService;
import br.unb.comnet.streaming.distranb.services.PlayListBuilder;

@RestController
@CrossOrigin
public class DistranBController {
	private Log log = LogFactory.getLog(DistranBController.class);
	
	@Autowired
	private TranscoderRepository repoTranscoders;
	
	@Autowired
	private FileVideoService fileVideoService;
	
	@Autowired
	private TaskService taskService;
	
	@PostMapping("/jobs/start")
	@ResponseStatus(HttpStatus.CREATED)
	public String startAJob(@RequestParam("m3u8url") String m3u8url) {
		try {
			PlayListBuilder task = taskService.startATask(m3u8url);
			return task.getName() + " was created.";
		} catch (TaskServiceException | IOException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());			
		}
	}
	
	@GetMapping("/jobs/{jobname}/stop")
	public String stopAJob(@PathVariable("jobname") String jobName) {
		try {
			Optional<PlayListBuilder> opTask = taskService.stopATask(jobName);
			if (opTask.isEmpty()) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Task " + jobName + " not found.");
			}
			return "Job " + jobName + " was told to stop.";				
		} catch (TaskServiceException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "A job name must be provided.");
		}
	}
	
	@GetMapping("/jobs")
	public List<String> jobsPlaying() {
		return taskService.listTaskNames();
	}
	
	@GetMapping(value="videos/list", produces="application/json")
	public List<String> getFileList() {
		try {
			return fileVideoService.listFiles("m3u8");
		} catch (IOException e) {
			log.error("There is a problem! >>> {}", e);
			return null;
		}
	}
	
	@GetMapping("/videos/full/{fileName}")
	public ResponseEntity<Resource> getFullVideo(@PathVariable("fileName") String fileName) {
		try {
			log.info("Serving file " + fileName);
			Resource resource = fileVideoService.getVideoURL(fileName);
			MediaType mediaType = fileVideoService.getResourceMediaType(resource);
			
			return ResponseEntity
					.status(HttpStatus.OK)
					 .header(HttpHeaders.CONTENT_DISPOSITION, "inline;filename=" + fileName)
					.contentType(mediaType)
					.cacheControl(getCacheControl(mediaType))
					.body(resource);			
		} catch (IOException e) {
			log.error("There is a problem! >>> {}", e);
			return ResponseEntity
					.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(null);
		}
	}	
	
	@PostMapping("/register")
	public RegisterInfo registerTranscoder(@RequestBody RegisterInfo registerInfo) {
		log.info("Receiving registration " + registerInfo.mountUrl());
		
		TranscoderClient client = 
				new TranscoderClient(
					registerInfo.getName(), 
					registerInfo.mountUrl()
				);
		
		repoTranscoders.add(client);
		
		return registerInfo;
	}
	
	@PostMapping("/feedback")
	public void receiveFeedback(@RequestBody List<UtilityFeedback> feedbacks) {
		taskService.processUtilityFeedback(feedbacks);
	}
	
	private CacheControl getCacheControl(MediaType mediaType) {
		if (mediaType.getSubtype().equals("vnd.apple.mpegurl")) {
			return CacheControl.noStore().cachePrivate();
		}
		return CacheControl.empty();
	}	
}
