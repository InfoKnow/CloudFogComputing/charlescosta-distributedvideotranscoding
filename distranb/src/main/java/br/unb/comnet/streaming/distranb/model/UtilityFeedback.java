package br.unb.comnet.streaming.distranb.model;

import java.io.Serializable;
import java.time.LocalDateTime;

public class UtilityFeedback implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String evaluator;
	private Integer id;
	private String url;
	private Double utility;
	private LocalDateTime timeStamp;
	
	public String getEvaluator() {
		return evaluator;
	}
	public void setEvaluator(String evaluator) {
		this.evaluator = evaluator;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	public Double getUtility() {
		return utility;
	}
	public void setUtility(Double utility) {
		this.utility = utility;
	}
	
	public LocalDateTime getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(LocalDateTime timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	public UtilityFeedback() {}	
}
