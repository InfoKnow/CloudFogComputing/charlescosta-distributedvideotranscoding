package br.unb.comnet.streaming.distranb.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import br.com.tm.repfogagent.trm.FIREtrm;
import br.com.tm.repfogagent.trm.Rating;
import br.com.tm.repfogagent.trm.components.InteractionTrustComponent;
import br.com.tm.repfogagent.trm.components.WitnessReputationComponent;
import br.unb.comnet.streaming.distranb.model.TranscoderClient;

public class ModifiedFIRETranscoderEvaluator {
	
	Log logger = LogFactory.getLog(ModifiedFIRETranscoderEvaluator.class);	
	
	public void evaluateTranscoders(Collection<TranscoderClient> transcoders, int numOfSegments) {

		StringBuilder str = new StringBuilder("\r\n---\r\n");
		
		for(TranscoderClient transInfo : transcoders) {
			if (!transInfo.getRatings().isEmpty()) {
				
				List<Rating> local = new ArrayList<>();
				List<Rating> supportingRatings = new ArrayList<>();				
				Map<String, List<Rating>> ratingsPerNode = new LinkedHashMap<>();
				
				splitRatings(transInfo.getRatings(), local, supportingRatings, ratingsPerNode, numOfSegments);
				
				if (!local.isEmpty() && !supportingRatings.isEmpty()) {
					WitnessReputationComponent witnessComponent = new WitnessReputationComponent(0, 0.4, 1.0, ratingsPerNode, local);
					
					double witnessValue = witnessComponent.calculate(supportingRatings, supportingRatings.size());
					double reliabilityWR = witnessComponent.reliability(supportingRatings);
					
					witnessComponent.setCalculatedValue(witnessValue);
					witnessComponent.setCalculatedReliability(reliabilityWR);
					
					InteractionTrustComponent directComponent = new InteractionTrustComponent(0,  0.8);				
					
					if (!local.isEmpty()) {
						double interactionTrustValue = directComponent.calculate(local, local.size());
						double reliabilityIT = directComponent.reliability(local);
						
						directComponent.setCalculatedValue(interactionTrustValue);
						directComponent.setCalculatedReliability(reliabilityIT);					
					} else {
						directComponent.setCalculatedValue(1.0);
						directComponent.setCalculatedReliability(1.0);					
					}
					
					FIREtrm firEtrm = new FIREtrm(Arrays.asList(witnessComponent, directComponent));
					Double fireValue = firEtrm.calculate();
					Double overallReliability = firEtrm.reliability();				
					
					transInfo.setTrustworthy(fireValue);
					transInfo.setReliability(overallReliability);

					str.append("Trustworthy of " + transInfo.getName() + " is " + fireValue + "\r\n");				
				}
			}
		}
		
		str.append("\r\n---\r\n");
		logger.info(str.toString());					
	}
	
	private void splitRatings(
			Map<String, Set<Rating>> origin, 
			List<Rating> direct, 
			List<Rating> supportingRatings, 
			Map<String, List<Rating>> ratingsPerNode,
			int numOfSegments
	) {
		Map<String, List<Rating>> source = new LinkedHashMap<>();
		for(String key : origin.keySet()) {
			source.put(key, new ArrayList<>(origin.get(key)));
		}
		
		direct.addAll(calculateAverageRating(origin, numOfSegments));
		
		if (!direct.isEmpty()) {
			for(Rating directRating : direct) {
				for(String key : source.keySet()) {
					for(Rating indirectRating : source.get(key)) {
						if (indirectRating.getIteration() == directRating.getIteration()) {
							if (!ratingsPerNode.containsKey(key)) {
								ratingsPerNode.put(key, new ArrayList<Rating>());
							}
							ratingsPerNode.get(key).add(indirectRating);
							supportingRatings.add(indirectRating);
						}
					}
				}
			}
		}
	}
	
	private List<Rating> calculateAverageRating(Map<String, Set<Rating>> origin, int numOfSegments) {
		List<Rating> averageRatings = new ArrayList<>();
		
		for (int i = 0; i <= numOfSegments; i++) {
			int qtd= 0;
			double value = 0D;
			String serverName = null;
			
			for(Set<Rating> ratings : origin.values()) {
				for(Rating rating : ratings) {
					if (rating.getIteration() == i) {
						if (serverName == null) {
							serverName = rating.getServerName();
						}
						qtd++;
						value += rating.getNormalizedValue();
					}
				}
			}
			
			if (qtd > 0) {
				Rating avgRating = new Rating();
				avgRating.setServerName(serverName);
				avgRating.setIteration(i);
				avgRating.setNormalizedValue(value / qtd);
				
				averageRatings.add(avgRating);				
			}
		}
		
		return averageRatings;
	}

}
