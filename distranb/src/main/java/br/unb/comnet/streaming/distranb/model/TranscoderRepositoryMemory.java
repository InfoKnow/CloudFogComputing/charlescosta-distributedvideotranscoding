package br.unb.comnet.streaming.distranb.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class TranscoderRepositoryMemory implements TranscoderRepository {
	
	private List<TranscoderClient> transcoders;
	
	public TranscoderRepositoryMemory() {
		transcoders = new ArrayList<>();
	}

	@Override
	public void add(TranscoderClient transcoder) {
		Objects.requireNonNull(transcoder, "Transcoder cannot be null");
		transcoders.add(transcoder);
	}

	@Override
	public int size() {
		return transcoders.size();
	}

	@Override
	public Optional<TranscoderClient> getTranscoder(int index) {
		if (index < 0 || index >= transcoders.size()) {
			return Optional.empty();
		}
		return Optional.of(transcoders.get(index));
	}
	
	@Override
	public List<TranscoderClient> listAllTranscoders() {
		return new ArrayList<>(transcoders);
	}

	@Override
	public List<TranscoderInfo> listTranscodersInfo() {
		return transcoders.stream()
				.map(TranscoderClient::getTranscoderInfo)
				.collect(Collectors.toList());
	}

	@Override
	public Optional<TranscoderClient> getResponsibleFor(String url) {
		return transcoders.stream().filter(x->x.isResponsibleFor(url)).findAny();
	}
}
