package br.unb.comnet.streaming.distranb.model;

import java.util.List;
import java.util.Optional;

import br.unb.comnet.streaming.distranb.services.PlayListBuilder;

public interface TaskRepository {
	
	public List<PlayListBuilder> listTasks();
	public Optional<PlayListBuilder> getTask(String name);
	public PlayListBuilder addTask(PlayListBuilder task);
	public Optional<PlayListBuilder> removeTask(String name);
	public int numOfTasks();

}
