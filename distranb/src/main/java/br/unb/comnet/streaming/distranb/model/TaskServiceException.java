package br.unb.comnet.streaming.distranb.model;

public class TaskServiceException extends Exception {
	private static final long serialVersionUID = 1L;

	public TaskServiceException(String msg) {
		super(msg);
	}
}
