package br.unb.comnet.streaming.distranb.services;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.Resource;

import br.unb.comnet.streaming.distranb.flexM3u8IO.MediaSegment;
import br.unb.comnet.streaming.distranb.flexM3u8IO.PlayList;
import br.unb.comnet.streaming.distranb.model.TranscoderClient;
import br.unb.comnet.streaming.distranb.model.TranscoderRepository;

public class TaskDistributionUnit extends PlayListBuilder {
	private Log log = LogFactory.getLog(TaskDistributionUnit.class);
	
	private URL m3u8Source;
	private TranscoderRepository repo;
	
	PlayList inputPlayList;
	PlayList outputPlayList;
	
	Resource output;
	
	private List<String> already;
	
	private Integer numOfSegments;
	private ModifiedFIRETranscoderEvaluator transcoderEvaluator;
	
	public TaskDistributionUnit(
			String name,
			FileVideoService fileService,
			TranscoderRepository repo,
			URL m3u8Source, 
			Resource output
	){
		super(name, fileService);
		this.repo = repo;
		this.m3u8Source = m3u8Source;
		this.output = output;
	}

	@Override
	public void initilize() {
		try {
			log.info("Will write to the file " + output.getFilename());		
			already = new ArrayList<String>();
			
			log.info("Reading the input file " + m3u8Source.getFile());
			this.inputPlayList = readInputPlayList();
			this.outputPlayList = inputPlayList.cloneWithoutSegments();
			
			this.numOfSegments = 0;
			this.transcoderEvaluator = new ModifiedFIRETranscoderEvaluator();			
		} catch (IOException e) {
			log.error("Something wrong has happen >> {}", e);
		}
	}
	
	@Override
	public void process() {
		try {
			if (numOfSegments > 0) {
				transcoderEvaluator.evaluateTranscoders(repo.listAllTranscoders(), numOfSegments);				
			}
			
			Instant start = Instant.now();
			
			MediaSegment nextSegment = getNextSegment(inputPlayList.getSegments());
			while (nextSegment != null) {

				int index = chooseNodeIndex();
				
				log.info(
						" ** Segment " + nextSegment.getUrl() + 
						" goes to transcoder " + 
						index + "."
				);
				
				Optional<TranscoderClient> client = getClient(index);
				if (client.isPresent()) {
					String newURL = client.get().transcodeTo720p(nextSegment.getUrl());
					if (newURL != null) {
						numOfSegments++;
						client.get().addSegment(newURL);
						outputPlayList.addSegment(newURL, nextSegment.getDuration(), nextSegment.getInfo());
						log.info("The new video file is " + newURL);						
					} else {
						log.info("Ops! Something went wrong!");
					}
				}
				
				nextSegment = getNextSegment(inputPlayList.getSegments());				
			}
			
			log.info("Updating destiny...");
			writeOut(outputPlayList, output);
			
			log.info("Updating source...");
			inputPlayList = readInputPlayList();
			
			Duration elapsed = Duration.between(start, Instant.now());
			log.info("Elapsed time to transcode is " + Integer.valueOf(elapsed.getNano() / 1000000) + " miliseconds. ");			
		} catch(IOException e) {
			log.error("Something wrong has happen >> {}", e);			
			stop();
		}
	}	
	
	private PlayList readInputPlayList() throws IOException {
		try (InputStream stream = m3u8Source.openStream()) {
			return getReader().read(stream);
		} catch (IOException e) {
			throw e;
		}
	}
	
	private int chooseNodeIndex() {
		return new Random().nextInt(repo.size());
	}
	
	private void writeOut(PlayList playList, Resource output) throws IOException {
		try (OutputStream outputStrean = new FileOutputStream(output.getFile())) {
			getWriter().write(playList, outputStrean);			
			playList.incrementMediaSequence();			
		} catch(IOException e) {
			throw e;
		}
	} 
	
	private MediaSegment getNextSegment(List<MediaSegment> segments) {
		for(MediaSegment segment : segments) {
			if (!already.contains(segment.getUrl())) {
				already.add(segment.getUrl());
				return segment;
			}
		}
		return null;
	}
	
	public Optional<TranscoderClient> getClient(int index) {
		return repo.getTranscoder(index);
	}
}
