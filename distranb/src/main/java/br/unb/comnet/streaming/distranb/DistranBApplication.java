package br.unb.comnet.streaming.distranb;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import br.unb.comnet.streaming.distranb.model.TaskRepository;
import br.unb.comnet.streaming.distranb.model.TaskRepositoryMemory;
import br.unb.comnet.streaming.distranb.model.TaskService;
import br.unb.comnet.streaming.distranb.model.TranscoderRepository;
import br.unb.comnet.streaming.distranb.model.TranscoderRepositoryMemory;
import br.unb.comnet.streaming.distranb.services.FileVideoService;

@SpringBootApplication
public class DistranBApplication {

	public static void main(String[] args) {
		SpringApplication.run(DistranBApplication.class, args);
	}
	
	@Value("${videoDirectory}")
	private String videosDirectory;	
	
	@Bean
	public FileVideoService getFileVideoService() {
		return new FileVideoService(videosDirectory);
	}		
	
	@Bean
	public TranscoderRepository getTranscoderRepository() {
		return new TranscoderRepositoryMemory();
	}
	
	@Bean
	public TaskRepository getTaskRepository() {
		return new TaskRepositoryMemory();
	}
	
	@Bean 
	public TaskService getTaskService(
		TaskRepository repoTask, 
		TranscoderRepository repoTranscoder, 
		FileVideoService fileService
	) {
		return new TaskService(repoTask, repoTranscoder, fileService);
	}
}
