package br.unb.comnet.streaming.distranb.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import br.unb.comnet.streaming.distranb.services.PlayListBuilder;

public class TaskRepositoryMemory implements TaskRepository {
	
	private Map<String, PlayListBuilder> tasks;
	
	public TaskRepositoryMemory() {
		this.tasks = new ConcurrentHashMap<>();
	}

	@Override
	public List<PlayListBuilder> listTasks() {
		return new ArrayList<>(tasks.values());
	}

	@Override
	public Optional<PlayListBuilder> getTask(String name) {
		return Optional.ofNullable(tasks.get(name));
	}

	@Override
	public PlayListBuilder addTask(PlayListBuilder task) {
		Objects.requireNonNull(task, "Task must be not null");
		return tasks.put(task.getName(), task);
	}

	@Override
	public Optional<PlayListBuilder> removeTask(String name) {
		return Optional.ofNullable(tasks.remove(name));
	}

	@Override
	public int numOfTasks() {
		return tasks.size();
	}
}
