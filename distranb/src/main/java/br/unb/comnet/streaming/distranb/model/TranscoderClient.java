package br.unb.comnet.streaming.distranb.model;

import java.net.MalformedURLException;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.util.Strings;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

import br.com.tm.repfogagent.trm.Rating;

public class TranscoderClient {
	
	private final String name;
	private final String clientUrl;
	private RestTemplate restTemplate;
	
	private Set<String> transcodedSegments;
	
	private Map<String, Set<Rating>> ratings;
	private Double totalUtility = 0.0;
	private Double trustworth = 0.5;
	private Double reliability = 0.0;	
	
	public String getName() {
		return name;
	}
		
	public TranscoderClient(String name, String url) {
		this.name = name;
		this.clientUrl = url;
		this.restTemplate = new RestTemplate();
		this.transcodedSegments = new HashSet<>();
		this.ratings = new ConcurrentHashMap<>();
	}
	
	public int countSegments() {
		return transcodedSegments.size();
	}
	
	public String addSegment(String url) {
		if (!Strings.isBlank(url)) {
			transcodedSegments.add(url);			
		}
		return url;
	}
	
	public boolean isResponsibleFor(String url) {
		return transcodedSegments.contains(url);
	}
	
	/**
	 * Require a video to be transcoded to 480p and sets its output name
	 * @param fileUrl
	 * @param outputName
	 * @return
	 */
	public String transcodeTo720p(String fileUrl) {
		return transcode(0, fileUrl);
	}
	
	/**
	 * Require a video to be transcoded to 480p and sets its output name
	 * @param url
	 * @param outputName
	 * @return
	 * @throws MalformedURLException 
	 */
	public String transcodeTo480p(String fileUrl) {
		return transcode(1, fileUrl);
	}
	
	/**
	 * Require a video to be transcoded to 480p and sets its output name 
	 * @param url
	 * @param outputName
	 * @return
	 * @throws MalformedURLException
	 */
	public String transcodeTo360p(String fileUrl) {
		return transcode(2, fileUrl);
	}	
	
	@SuppressWarnings("unchecked")
	public List<String> listFiles() {
		return (List<String>) restTemplate.getForObject(clientUrl + "/videos/list", List.class);
	}
	
	public Map<String, Set<Rating>> getRatings() {
		return new LinkedHashMap<String, Set<Rating>>(ratings);
	}
	
	public Integer getNumOfRatings() {
		int totalSize = 0;
		for(String key : ratings.keySet()) {
			totalSize += ratings.get(key).size();
		}
		return totalSize;
	}
	
	public void addRating(Rating rating) {
		if (!ratings.containsKey(rating.getServerName())) {
			ratings.put(rating.getServerName(), new TreeSet<Rating>(new Comparator<Rating>() {
				@Override
				public int compare(Rating o1, Rating o2) {
					return Integer.valueOf(o1.getIteration()).compareTo(o2.getIteration());
				}
			}));
		}
		
		ratings.get(rating.getServerName()).add(rating);
		setTotalUtility(getTotalUtility() + rating.getValue());
	}
	
	public Double getTotalUtility() {
		return totalUtility;
	}
	public void setTotalUtility(Double totalUtility) {
		this.totalUtility = totalUtility;
	}

	public Double getTrustworth() {
		return trustworth;
	}
	public void setTrustworthy(Double trustworthy) {
		this.trustworth = trustworthy;
	}
	
	public Double getReliability() {
		return reliability;
	}
	public void setReliability(Double reliability) {
		this.reliability = reliability;
	}
	
	public TranscoderInfo getTranscoderInfo() {
		return new TranscoderInfo(
			getName(), 
			transcodedSegments.size(), 
			getTotalUtility(), 
			getTrustworth(), 
			getReliability()
		);
	}
	
	private String transcode(int frameSize, String fileUrl) {
		JobOffer jobOffer = new JobOffer();
		jobOffer.setUrl(fileUrl);
		jobOffer.setFrameSize(0);
		HttpEntity<JobOffer> request = new HttpEntity<>(jobOffer);
		
		jobOffer = restTemplate.postForObject(clientUrl + "/offer", request, JobOffer.class);
		if (jobOffer.getAccepted() && jobOffer.getReady()) {
			return clientUrl + "/videos/full/" + jobOffer.getOutputFileName();
		}
		
		return null;
	}
}
