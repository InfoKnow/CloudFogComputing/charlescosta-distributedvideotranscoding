//
// Veja https://github.com/video-dev/hls.js/blob/master/docs/API.md#runtime-events
// sobre informações sobre os eventos do hls.js
//
const UTIL_BETHA = 1000;

class SegmentInfo {
	constructor(id, url, duration) {
		this._id = id;
		this._url = url;
		this._duration = duration * 1000;
		this._length = 1024 * 1024;
		this._requestingTime = new Date();
		this._receivingTime = null;
	}
	
	get id() {
		return this._id;
	}
	
	get url() {
		return this._url;
	}
	
	get duration() {
		return this._duration;
	}
	
	get length() {
		return this._length;
	}
	
	get requestingTime() {
		return this._requestingTime;
	}
	
	get receivingTime() {
		if (this._receivingTime == null) {
			return new Date();
		}
		return this._receivingTime;
	}
	
	servingInterval() {
		return this.receivingTime - this.requestingTime;
	}
	
	success() {
		return (this.servingInterval() <= (this._duration * 5)) ? 1.0 : 0.0;
	}
	
	markReceived(length, time) {
		this._length = length;
		this._receivingTime = time;
	}
	
	standardUtility() {
		var max = this.calculateMaxUtility();
		var min = -5 * max;
		var utility = this.calculateUtility();
		var standard = (utility - min) / (max - min);
		return Math.max(standard, 0.0);		
	}
	
	calculateMaxUtility() {
		return (this._length + UTIL_BETHA * this._duration) / this._duration;
	}
	
	calculateUtility() {
		const servingInterval = this.servingInterval();
		const success = this.success();
		
		return ((this._length * success) + UTIL_BETHA * ((this._duration * success) - servingInterval)) / this._duration;
	}
}

class UtilityFeedback {
	constructor(evaluator, id, url, utility, timeStamp) {
		this.evaluator = evaluator;
		this.id = id;
		this.url = url;
		this.utility = utility;
		this.timeStamp = timeStamp;
	}
}

class UtilityTracker {
	constructor(evaluator) {
		this._evaluator = evaluator;
		this._segments = [];
	}
	
	addSegment(url, duration) {
		if (url && !this.containsSegment(url)) {
			var seg = new SegmentInfo(this._segments.length, url, duration);
			this._segments.push(seg);
		}
	}
	
	containsSegment(url) {
		return this.getSegment(url) != null;
	}		
	
	getSegment(url) {
		if (url) {
			const seg = this._segments.find(x => x.url === url);
			return seg;
		}
		return null;			
	}
	
	markSegmentReceived(url, length) {
		var seg = this.getSegment(url);
		if (seg) {
			seg.markReceived(length, new Date());
			return seg;
		}
		return null;
	}
	
	aggregateUtilityFeedback() {
		const name = this._evaluator;
		
		return this._segments.map(function(x) {
			return new UtilityFeedback(
				name, 
				x.id, 
				x.url, 
				x.standardUtility(), 
				x.receivingTime
			);
		});
	}
}
