package br.unb.comnet.streaming.distrant.services;

import java.io.IOException;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.Resource;

import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.builder.FFmpegBuilder;

public class FFmpegService {
	private final Log log = LogFactory.getLog(FFmpegService.class);
	
	private FFmpegExecutor executor;
	
	public FFmpegService(FFmpegExecutor executor) {
		this.executor = executor;
	}
	
	public void transcode(Resource resource) throws IOException {
		executor.createJob(createTwoFilesBuilder(resource.getFile().getAbsolutePath())).run();		
	}
	
	public void reduceQuality(String input, String outputName, int crf) throws IOException {
		log.info("Starting reducing " + input + " by CRF of " + crf);
		long start = System.currentTimeMillis();
		
		executor
			.createJob(reduceBitRate(crf, input, outputName))
				.run();
		
		long end = System.currentTimeMillis();
		log.info("Ellapsed time on reduceQuality is " + (end - start) + "ms.");		
	}
	
	public void resizeTo720p(String input, String outputName) throws IOException {
		log.info("Starting resizing " + input + " to 1280x720p... ");
		long start = System.currentTimeMillis();
		
		executor
			.createJob(resize(input, outputName, 1280, 720))
				.run();
		
		long end = System.currentTimeMillis();
		log.info("Ellapsed time on resizeTo720p is " + (end - start) + "ms.");
	}	
	
	public void resizeTo480p(String input, String outputName) throws IOException {
		log.info("Starting resizing " + input + " to 720x480p... ");
		long start = System.currentTimeMillis();
		
		executor
			.createJob(resize(input, outputName, 720, 480))
				.run();
		
		long end = System.currentTimeMillis();
		log.info("Ellapsed time on resizeTo480p is " + (end - start) + "ms.");
	}
	
	public void resizeTo360p(String input, String outputName) throws IOException {
		log.info("Starting resizing " + input + " to 640x360p... ");
		long start = System.currentTimeMillis();
		
		executor
			.createJob(resize(input, outputName, 640, 360))
				.run();
		
		long end = System.currentTimeMillis();
		log.info("Ellapsed time on resizeTo360p is " + (end - start) + "ms.");		
	}
	
	public void changeBitrate(String input, String output) throws IOException {
		log.info("Starting changing bitrate of " + input + " to 3500... ");
		long start = System.currentTimeMillis();
		
		executor
			.createJob(changeBitrate(3500, input, output))
				.run();
		
		long end = System.currentTimeMillis();
		log.info("Ellapsed time on changeBitrate is " + (end - start) + "ms.");		
	}
	
	public void applyDefaultFilter(String input, String output) throws IOException {
		log.info("Starting applying default filter to " + input + "... ");
		long start = System.currentTimeMillis();
		
		executor
			.createJob(defaultFilter(input, output))
				.run();
		
		long end = System.currentTimeMillis();
		log.info("Ellapsed time on applyDefaultFilter is " + (end - start) + "ms.");		
	}
	
	public void slice2sTo1s(String input, String output) {
		log.info("Starting slicing 2s video to two 1s videos from " + input + "... ");
		long start = System.currentTimeMillis();
		
		executor.createJob(slice(input, 0f, 0.95f, 0, "p0_" + output)).run();
		executor.createJob(slice(input, 1f, 2f, 0, "p1_" + output)).run();		
		
		long end = System.currentTimeMillis();
		log.info("Ellapsed time on slice2sTo1s is " + (end - start) + "ms.");		
	}
	
	public void sliceIntoParts(String input, float duration, float step, float startTime, String output) {
		log.info("Starting slicing " + input + "into parts... ");
		long startLog = System.currentTimeMillis();		
		
		float start = 0;
		float end = step;
		float time = startTime;
		int i = 0;
		while (start < duration) {
			executor
				.createJob(
						slice(
							input, 
							start, 
							end, 
							time,
							"p" + i + "_" + output
						)
					).run();
			start += step;
			end += step;
			time += step;
			i++;
		}
		
		long endLog = System.currentTimeMillis();
		log.info("Ellapsed time on sliceIntoParts is " + (endLog - startLog) + "ms.");			
	}
	
	private FFmpegBuilder resize(String input, String outputName, int width, int height) {
		FFmpegBuilder builder = new FFmpegBuilder()
				.setVerbosity(FFmpegBuilder.Verbosity.QUIET)
				  .setInput(input)
				  .addOutput(outputName)
				  	.setVideoFilter("\"scale=" + width + ":" + height + "\"")
				  	.addExtraArgs("-copyts")	
				  	.addExtraArgs("-muxdelay", "0")				  	
				  .done();
		return builder;		
	}
	
	private FFmpegBuilder createTwoFilesBuilder(String fileName) {
		FFmpegBuilder builder = new FFmpegBuilder()
				  .setVerbosity(FFmpegBuilder.Verbosity.INFO)
				  .setInput("\"" + fileName + "\"")
				  .addOutput("\"" + fileName + "_.m3u8\"")
				  	.addExtraArgs("-crf", "21")
				  	.setAudioCodec("aac")
				  	.setAudioBitRate(128000)
				  	.setAudioChannels(2)
				  	.setPreset("superfast")
				  	.setFormat("hls")
				  	.addExtraArgs("-hls_time", "2")
				  	.addExtraArgs("-hls_playlist_type", "event ")
				  .done();
		return builder;
	}
	
	private FFmpegBuilder reduceBitRate(int factor, String input, String output) {
		FFmpegBuilder builder = new FFmpegBuilder()
				.setVerbosity(FFmpegBuilder.Verbosity.QUIET)
				  .setInput(input)
				  .addOutput(output)
				  	.setVideoCodec("libx264")				  
				  	.addExtraArgs("-crf", String.valueOf(factor))
				  	.addExtraArgs("-copyts")	
				  	.addExtraArgs("-muxdelay", "0")				  	
				  	.setAudioCodec("aac")
				  	.setPreset("ultrafast")
				  .done();
		return builder;
	}
	
	private FFmpegBuilder changeBitrate(int bitrate, String input, String output) {
		FFmpegBuilder builder = new FFmpegBuilder()
				.setVerbosity(FFmpegBuilder.Verbosity.QUIET)
				  .setInput(input)
				  .addOutput(output)
				  	.addExtraArgs("-b", Integer.toString(bitrate))
				  	.addExtraArgs("-copyts")	
				  	.addExtraArgs("-muxdelay", "0")				  	
				  .done();
		return builder;		
	}
	
	private FFmpegBuilder defaultFilter(String input, String output) {
		FFmpegBuilder builder = new FFmpegBuilder()
				.setVerbosity(FFmpegBuilder.Verbosity.QUIET)
				  .setInput(input)
				  .addOutput(output)
				  	.addExtraArgs("-copyts")	
				  	.addExtraArgs("-muxdelay", "0")				  	
				  .done();
		return builder;		
	}	
	
	private FFmpegBuilder slice(String input, float start, float stop, float startTime, String output) {
		FFmpegBuilder builder = new FFmpegBuilder()
				.setVerbosity(FFmpegBuilder.Verbosity.QUIET)
				  .addExtraArgs("-ss", formatFloat(start))
				  .setInput(input)
				  .addOutput(output)
				  	.addExtraArgs("-t", formatFloat(stop * 0.95))
				  	.addExtraArgs("-c", "copy")				  
				  	//.addExtraArgs("-muxdelay", formatFloat(startTime))				  	
				  .done();
		return builder;	
	}
	
	private String formatFloat(double number) {
		return String.format(Locale.US, "%.2f", number);
	}
}
