package br.unb.comnet.streaming.distrant;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.context.ServletWebServerApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

import br.unb.comnet.streaming.distrant.services.FFmpegService;
import br.unb.comnet.streaming.distrant.services.FileVideoService;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.RunProcessFunction;

@SpringBootApplication
public class DistranTApplication {
	
	private Log log = LogFactory.getLog(DistranTApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(DistranTApplication.class, args);
	}
	
	@Value("${videoDirectory}")
	private String videosDirectory;
	
	@Value("${ffmpegPath}")	
	private String ffmpegPath;
	
	@Value("${ffprobPath}")	
	private String ffprobPath;
	
	@Value("${brokerUrl}")
	private String brokerUrl;
	
	@Autowired
	private ServletWebServerApplicationContext serverProperties;
	
	@Bean
	public FileVideoService getFileVideoService() {
		return new FileVideoService(videosDirectory);
	}
	
	@Bean
	public FFmpegService getFFmpegService(FFmpeg ffmpeg, FFprobe ffprobe) throws IOException {
		return new FFmpegService(new FFmpegExecutor(ffmpeg, ffprobe));
	}
	
	@Bean
	public FFprobe getFFProbe(RunProcessFunction runProcessFunction) {
		return new FFprobe(ffprobPath, runProcessFunction);
	}
	
	@Bean
	public FFmpeg getFFmpeg(RunProcessFunction runProcessFunction) throws IOException {
		return new FFmpeg(ffmpegPath, runProcessFunction);
	}
	
	@Bean
	public RunProcessFunction getRunProcessFunction() throws IOException {
		RunProcessFunction runProcessFunction = new RunProcessFunction();
		runProcessFunction.setWorkingDirectory(videosDirectory);		
		return runProcessFunction;
	}
	
	@EventListener
	public void onApplicationEvent(ContextRefreshedEvent event) {
		try {
			log.info("Registering into broker " + brokerUrl);
			registerOnBroker(
					String.valueOf(serverProperties.getWebServer().getPort()), 
					InetAddress.getLocalHost().getHostAddress(), 
					brokerUrl
			);
		} catch (UnknownHostException e) {
			log.error("Could not get the current IP address. Error: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	private void registerOnBroker(String port, String ip, String brokerUrl) {
		RegisterInfo info = new RegisterInfo();
		info.setIp(ip);
		info.setPort(port);
		info.setName("transcoder" + System.currentTimeMillis());
		
		HttpEntity<RegisterInfo> request = new HttpEntity<>(info);
		new RestTemplate().postForObject(brokerUrl + "/register", request, RegisterInfo.class);
	}

}
