package br.unb.comnet.streaming.distrant.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.List;

import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourceRegion;
import org.springframework.http.HttpRange;
import org.springframework.http.MediaType;
import org.springframework.http.MediaTypeFactory;

public class FileVideoService {
	private static final long MAX_DATA_BLOCK = 1024 * 1024;
	
	private String videosDirectory;
	
	public FileVideoService(String videosDirectory) {
		this.videosDirectory = videosDirectory;
	}
	
	public List<String> listFiles(final String... extensions) throws MalformedURLException, IOException {
		File directory = new File(getVideoURL(".").getURI());
		return Arrays.asList(directory.list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				for(String extension: extensions) {
					if (name.toLowerCase().endsWith("." + extension.toLowerCase())) {
						return true;
					}
				}
				return false;
			}
		}));
	}
	
	public Resource getVideoURL(String fileName) throws MalformedURLException, IOException {
		Resource input = getInputResource(fileName);
		if (!input.exists()) {
			return null;
		}
		return input;			
	}

	public MediaType getResourceMediaType(Resource resource) {
		return MediaTypeFactory
				.getMediaType(resource)
				.orElse(MediaType.APPLICATION_OCTET_STREAM);
	}
	
	public ResourceRegion getResourceRegion(Resource resource, HttpRange range) throws IOException {
		if (range == null) {
			return new ResourceRegion(resource, 0, Math.min(MAX_DATA_BLOCK, resource.contentLength()));
		} else {
			long length = resource.contentLength();
			return new ResourceRegion(
					resource, 
					range.getRangeStart(length), 
					Math.min(MAX_DATA_BLOCK, range.getRangeEnd(length) - range.getRangeStart(length) + 1)
			);
		}
	}
	
	public Resource createFileForWriting(String fileName) throws FileNotFoundException, IOException {
		Resource newFile = getInputResource(fileName);
		newFile.getFile().createNewFile();
		return newFile;
	}
	
	private Resource getInputResource(String fileName) {
		return new FileSystemResource(new File(videosDirectory + "/" + fileName));
	}
}
