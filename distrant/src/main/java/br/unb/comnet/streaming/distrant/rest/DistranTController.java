package br.unb.comnet.streaming.distrant.rest;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourceRegion;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRange;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import br.unb.comnet.streaming.distrant.services.FFmpegService;
import br.unb.comnet.streaming.distrant.services.FileVideoService;

@RestController
@CrossOrigin
public class DistranTController {
	private final Log log = LogFactory.getLog(DistranTController.class);
	
	private Map<String, Segment> segments = new ConcurrentHashMap<>();
	
	@Autowired
	private FFmpegService ffmpegService;
	
	@Autowired
	private FileVideoService fileService;
	
	@PostMapping("/offer")
	public JobOffer transcodificar(@RequestBody JobOffer job) throws IOException {
		Segment segment = createSegment(job);
		segments.put(segment.getName(), segment);
		Resource output = fileService.createFileForWriting(segment.getName());
		
		//if (job.getFrameSize() == 0) {
		//	ffmpegService.resizeTo480p(job.getUrl(), output.getFile().getPath());
		//}
		ffmpegService.reduceQuality(job.getUrl(), output.getFile().getPath(), 25);		
	
		job.setOutputFileName(segment.getName());
		job.setAccepted(true);
		job.setReady(true);
		return job;
	}
	
	@GetMapping(value="videos/list", produces="application/json")
	public List<String> getFileList() {
		try {
			return fileService.listFiles("mp4", "ts");
		} catch (IOException e) {
			log.error("There is a problem! >>> {}", e);
			return null;
		}
	}
	
	@GetMapping("/videos/full/{fileName}")
	public ResponseEntity<Resource> getFullVideo(@PathVariable("fileName") String fileName) {
		try {
			log.info("Serving file " + fileName);
			Resource resource = fileService.getVideoURL(fileName);
			MediaType mediaType = fileService.getResourceMediaType(resource);
			
			return ResponseEntity
					.status(HttpStatus.OK)
					 .header(HttpHeaders.CONTENT_DISPOSITION, "inline;filename=" + fileName)
					.contentType(mediaType)
					.cacheControl(getCacheControl(mediaType))
					.body(resource);			
		} catch (IOException e) {
			log.error("There is a problem! >>> {}", e);
			return ResponseEntity
					.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(null);
		}
	}	
	
	@GetMapping("/videos/partials/{fileName}")
	public ResponseEntity<ResourceRegion> getPartialVideo(
			@PathVariable("fileName") String fileName, 
			@RequestHeader HttpHeaders headers) 
	{
		return getVideoPartially(fileName, headers);
	}
	
	private ResponseEntity<ResourceRegion> getVideoPartially(String fileName, HttpHeaders headers) {
		try {
			log.info("Serving partialy file " + fileName);			
			Resource resource = fileService.getVideoURL(fileName);
			HttpRange range = headers.getRange().isEmpty() ? null : headers.getRange().get(0);
			return ResponseEntity
						.status(HttpStatus.PARTIAL_CONTENT)
						 .header(HttpHeaders.CONTENT_DISPOSITION, "inline;filename=" + fileName)						
						.contentType(fileService.getResourceMediaType(resource))
						.body(fileService.getResourceRegion(resource, range));
		} catch(IOException e) {
			log.error("There is a problem! >>> {}", e);
			return ResponseEntity
					.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(null);			
		}
	}	
	
	private CacheControl getCacheControl(MediaType mediaType) {
		if (mediaType.getSubtype().equals("vnd.apple.mpegurl")) {
			return CacheControl.noStore().cachePrivate();
		}
		return CacheControl.empty();
	}	
	
	private Segment createSegment(JobOffer job) throws MalformedURLException {
		Segment segment = new Segment();
		segment.setSource(new URL(job.getUrl()));
		segment.setName(generateTranscodedName(segment.getSource()));
		return segment;
	}
	
	private String generateTranscodedName(URL url) {
		return "t" + segments.size() + "_transcoded.ts";
	}
}
