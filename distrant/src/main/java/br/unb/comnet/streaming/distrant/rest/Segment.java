package br.unb.comnet.streaming.distrant.rest;

import java.net.URL;
import java.time.LocalDateTime;

public class Segment {
	
	private LocalDateTime creationTime;
	private URL source;
	private String transcodedFile;
	private Integer requestingCount;
	private String name;
	
	public Segment() {
		creationTime = LocalDateTime.now();
		requestingCount = 0;
	}
	
	public LocalDateTime getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(LocalDateTime creationTime) {
		this.creationTime = creationTime;
	}
	
	public URL getSource() {
		return source;
	}
	public void setSource(URL source) {
		this.source = source;
	}
	
	public String getTranscodedFile() {
		return transcodedFile;
	}
	public void setTranscodedFile(String transcodedFile) {
		this.transcodedFile = transcodedFile;
	}
	
	public Integer getRequestingCount() {
		return requestingCount;
	}
	public void setRequestingCount(Integer requestingCount) {
		this.requestingCount = requestingCount;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
